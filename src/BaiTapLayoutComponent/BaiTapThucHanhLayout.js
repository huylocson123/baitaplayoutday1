import React, { Component } from "react";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="container px-lg-5">
            <a class="navbar-brand" href="#!">
              Start Bootstrap
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#!">
                    Home
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#!">
                    About
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#!">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <header class="py-5">
          <div class="container px-lg-5">
            <div class="p-2 p-lg-5 bg-light rounded-3 text-center">
              <div class="m-4 m-lg-5">
                <h1 class="display-5 fw-bold">A warm welcome!</h1>
                <p class="fs-4">
                  Bootstrap utility classes are used to create this jumbotron
                  since the old component has been removed from the framework.
                  Why create custom CSS when you can use utilities?
                </p>
                <a class="btn btn-primary btn-lg" href="#!">
                  Call to action
                </a>
              </div>
            </div>
          </div>
        </header>
        <section className="pt-4">
          <div className="container px-lg-5">
            <div className="row gx-lg-5">
              <div className="col-lg-6 col-xxl-3 mb-5">
                <div className="card bg-light border-0 h-100">
                  <img src="logo192.png" className="card-img-top" alt="" />
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-3 mb-5">
                <div className="card bg-light border-0 h-100">
                  <img src="logo192.png" className="card-img-top" alt="" />
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-3 mb-5">
                <div className="card bg-light border-0 h-100">
                  <img src="logo192.png" className="card-img-top" alt="" />
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-3 mb-5">
                <div className="card bg-light border-0 h-100">
                  <img src="logo192.png" className="card-img-top" alt="" />
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="py-5 bg-dark">
          <div className="container">
            <p className="text-white text-center m-0 ">
              Copyright © Your Website 2022
            </p>
          </div>
        </div>
      </>
    );
  }
}

import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import DemoStyle from "./DemoStyle/DemoStyle";
import DataBinding from "./DataBinding/DataBinding";
import BaiTapThucHanhLayout from "./BaiTapLayoutComponent/BaiTapThucHanhLayout";
function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction />
      <DemoStyle /> */}
      {/* <DataBinding /> */}
      <BaiTapThucHanhLayout />
    </div>
  );
}

export default App;
